import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class App {
    public static void main(String[] args) throws Exception {
        String mail = "estamailnoexisteporquelodigoyo33@gmail.com";
        System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        driver.get("https://www.pccomponentes.com/intel-core-i3-7100-39ghz-box");

        // ! Imprimir título de la página
        String titulo = driver.getTitle();
        System.out.println(titulo);
        System.out.println("====================================================");

        // ! ===============  Pulsar Avisame  =============== ! //
        WebElement goToAvisameButton = driver.findElement(By.id("notify-me"));
        goToAvisameButton.click();
        Thread.sleep(3000);
        // ! ===============  <Escribir> Notificacion.  =============== ! //
        WebElement goToNotifyInput = driver.findElement(By.id("Notify_email"));
        goToNotifyInput.sendKeys(mail);
        Thread.sleep(3000);
        // ! ===============  <Pulsar Boton Check  =============== ! //
        WebElement condiciones = driver.findElement(By.className("c-indicator"));
        condiciones.click();
        Thread.sleep(3000);
        // ! ===============  <Pulsar Boton Enviar  =============== ! //
        WebElement buttonSend = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div[4]/div[1]/div[6]/div[2]/div/form/div[1]/div[3]/button"));
        buttonSend.click();
        Thread.sleep(3000);
 
        driver.quit();
    }
}