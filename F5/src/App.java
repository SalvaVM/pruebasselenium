import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class App {
    public static void main(String[] args) throws Exception {
        String username = "testejercicio@hotmail.com";
        String pass = "ejercicio11";
        String badNumber = "98874541A" ;

        System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        driver.get("https://www.pccomponentes.com/");

        // ! Imprimir título de la página
        String titulo = driver.getTitle();
        System.out.println(titulo);
        System.out.println("====================================================");

        // ! ===============  LOGIN  =============== ! //
        WebElement goToLoginButton = driver.findElement(By.className("qa-user-login-button"));
        goToLoginButton.click();

        WebElement usernameInput = driver.findElement(By.name("username"));
        WebElement passwordInput = driver.findElement(By.name("password"));
        WebElement loginButton = driver.findElement(By.xpath("/html/body/div[1]/main/div/div[2]/form/button[2]"));
        usernameInput.sendKeys(username);
        passwordInput.sendKeys(pass);
        loginButton.click();
        Thread.sleep(3000);

        // ! ===============  COOKIE  =============== ! //
        WebElement cookieButton = driver.findElement(By.className("accept-cookie"));
        cookieButton.click();
        Thread.sleep(3000);
        // ! ===============  IR A MI CUENTA =============== ! //
        WebElement menuButton = driver.findElement(By.id("user-menu-panel"));
        menuButton.click();
        Thread.sleep(3000);
        WebElement miCuentaButton = driver.findElement(By.className("qa-user-login-sub-4"));
        miCuentaButton.click();
        Thread.sleep(3000);
        // ! ===============  IR A AÑADIR DIRECCION =============== ! //
        WebElement anadirDirecButton = driver.findElement(By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div[9]/button"));
        anadirDirecButton.click();
        Thread.sleep(3000);
        // ! ===============  IR A AÑADIR TELEFONO INCORRECTO =============== ! //
        WebElement phoneInput = driver.findElement(By.id("userNewShippingAddress_cellPhone"));
        phoneInput.sendKeys(badNumber);
        phoneInput.sendKeys(Keys.RETURN);
        Thread.sleep(3000);

        driver.quit();
    }
}