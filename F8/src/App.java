import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class App {
    public static void main(String[] args) throws Exception {
        System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        driver.get("https://www.pccomponentes.com/");

        // ! Imprimir título de la página
        String titulo = driver.getTitle();
        System.out.println(titulo);
        System.out.println("====================================================");

        WebElement searchInput = driver.findElement(By.id("ais-autocomplete-selectized"));
        searchInput.sendKeys("@@@ºº!!");
        Thread.sleep(3000);
        searchInput.sendKeys(Keys.RETURN);
    
        Thread.sleep(3000);

        driver.quit();
    }
}