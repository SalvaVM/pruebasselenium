import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class App {
    public static void main(String[] args) throws Exception {
        System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
        String createmail = "estemailnoexisteporquelodigoyo892hotmail.com";
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.pccomponentes.com/");

        // ! Imprimir título de la página
        String titulo = driver.getTitle();
        System.out.println(titulo);
        System.out.println("====================================================");

        // ! ===============  LOGIN  =============== ! //
        WebElement goToLoginButton = driver.findElement(By.className("qa-user-login-button"));
        goToLoginButton.click();
        Thread.sleep(3000);
        // ! ===============  Pulsar Boton Crear =============== ! //
        WebElement clickCreateButton = driver.findElement(By.xpath("/html/body/div[1]/main/div/div[2]/form/a"));
        clickCreateButton .click();
        Thread.sleep(3000);
        // ! ===============  Introducir E-mail =============== ! //
        WebElement createMailInput = driver.findElement(By.xpath("/html/body/div[1]/main/div/div[2]/form/div[4]/div/input"));
        createMailInput.sendKeys(createmail);
        Thread.sleep(3000);
        driver.quit();
    }
}